//
//  TableViewplaceCell.swift
//  Health_management_copy_app
//
//  Created by ymikami on 2020/04/23.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit

class TableViewplaceCell: UITableViewCell {
    //日付
    @IBOutlet weak var time: UILabel!
    //場所
    @IBOutlet weak var place: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
