//
//  secondViewController.swift
//  Health_management_app
//
//  Created by ymikami on 2020/04/15.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit
import RealmSwift
import Charts

class secondViewController: UIViewController,UITextFieldDelegate  {

    // ①引数（文字列）
    var symptom = [String]()
//
    @IBOutlet weak var symptomLabel: UILabel!
    
    @IBOutlet weak var tbutton: UIButton!
    
    @IBOutlet weak var netu: UITextField!
    
    @IBOutlet weak var time: UITextField!
    
    @IBOutlet weak var place: UITextField!
    
    let maxLength: Int = 10
    //    @IBOutlet weak var memoText: UITextView!
    
    @IBOutlet weak var memoText: UITextField!
    
    //UIDatePickerを定義するための変数
    var datePicker: UIDatePicker = UIDatePicker()
    
    @IBAction func netuEditingChanged(_ sender: Any) {
        if netu.text == "" {
        tbutton.isEnabled = false
        } else {
        tbutton.isEnabled = true
        }
    }
    
    
    @IBAction func addButton(_ sender: Any) {
        
        
        
        // Itemクラスのインスタンス作成
        let newItem = ToDo()
        // TextFieldの値を代入
        newItem.netu = netu.text!
        newItem.time = time.text!
        newItem.place = place.text!
        newItem.memo = memoText.text!
        newItem.symptomm = symptomLabel.text!
//        newItem.date = Date()
        
        // インスタンスをRealmに保存
        do{
            
            let realm = try Realm()
            
            try realm.write({ () -> Void in
                realm.add(newItem, update:.all)
                
                print(time as Any)

            })

            
           
            
        }catch{
        }
        // 画面を閉じる
        self.dismiss(animated: true,completion: nil)
        
        
        
        
          
                        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         memoText.delegate = self

         // myTextFieldの入力チェック(文字数チェック)をオブザーバ登録
         NotificationCenter.default.addObserver(self,
                                                selector: #selector(textFieldDidChange(notification:)),
                                                name: UITextField.textDidChangeNotification,
                                                object: memoText)
        
        
        
         symptomLabel.text = symptom.joined(separator: ",")
        
        tbutton.isEnabled = false
        
        self.netu.keyboardType = UIKeyboardType.decimalPad
        // ピッカー設定
        datePicker.datePickerMode = UIDatePicker.Mode.date
        datePicker.timeZone = NSTimeZone.local
        datePicker.locale = Locale.current
        time.inputView = datePicker

        // 決定バーの生成
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 35))
        let spacelItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
        toolbar.setItems([spacelItem, doneItem], animated: true)

        // インプットビュー設定(紐づいているUITextfieldへ代入)
        time.inputView = datePicker
        time.inputAccessoryView = toolbar


        
        // Do any additional setup after loading the view.
    }
    
    // オブザーバの破棄
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // 入力チェック(文字数チェック)処理
    @objc func textFieldDidChange(notification: NSNotification) {
        let textField = notification.object as! UITextField

        if let text = textField.text {
            if textField.markedTextRange == nil && text.count > maxLength {
                textField.text = text.prefix(maxLength).description
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        netu.resignFirstResponder()
        return true
    }
    // UIDatePickerのDoneを押したら発火
    @objc func done() {
        time.endEditing(true)

        // 日付のフォーマット
        let formatter = DateFormatter()

        //"yyyy年MM月dd日"を"yyyy/MM/dd"したりして出力の仕方を好きに変更できる
        formatter.dateFormat = "yyyy年MM月dd日"

        //(from: datePicker.date))を指定してあげることで
        //datePickerで指定した日付が表示される
        time.text = "\(formatter.string(from: datePicker.date))"



    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    
    
    
    
    
   
    
          
          
          
        
    
   
   

}
