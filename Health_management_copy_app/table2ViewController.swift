//
//  table2ViewController.swift
//  Health_management_copy_app
//
//  Created by ymikami on 2020/04/22.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit
import RealmSwift
import Charts


class table2ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
var itemList: Results<ToDo>!
    
    @IBOutlet weak var tableplaceView: UITableView!
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableplaceView.delegate = self as UITableViewDelegate
        tableplaceView.dataSource = self as UITableViewDataSource
        
        
        // Realmからデータを取得
        do{
                   let realm = try Realm()
                   itemList = realm.objects(ToDo.self)
                //TableView日付順
                itemList = realm.objects(ToDo.self).sorted(byKeyPath: "time", ascending: false)
               }catch{
               }
        // tableViewにカスタムセルを登録
        tableplaceView.register(UINib(nibName: "TableViewplaceCell", bundle: nil), forCellReuseIdentifier: "TableViewplaceCell")
        
        tableplaceView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if ToDoFromDatabase().count <  32 {
        return itemList.count
        }else{
            return 31
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    // セルの内容を取得
    let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewplaceCell") as! TableViewplaceCell
        
        // カスタムセル内のプロパティ設定
            cell.place.text = itemList[indexPath.row].place
            cell.place.textAlignment = .center
            cell.place.adjustsFontSizeToFitWidth = true
            cell.time.text = itemList[indexPath.row].time
            cell.time.textAlignment = .center
            cell.time.adjustsFontSizeToFitWidth = true
            
            //セル反転
            //cell.transform = cell.transform.rotated(by: CGFloat(-Double.pi))
            
            

            return cell
        }
    
    //     画面が表示される直前にtableViewを更新
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableplaceView.reloadData()
        
    }
    
    //セルの削除
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath){
           if(editingStyle == UITableViewCell.EditingStyle.delete) {
               // Realm内のデータを削除
               do{
                   let realm = try Realm()
                   try realm.write {
                       realm.delete(self.itemList[indexPath.row])
                   }
                   tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
               }catch{
               }
            
           }
        tableView.reloadData()
       }
    let cellHeigh:CGFloat = 60
    
    // セルの高さを設定
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeigh
    }
    
    /// データベース（RealmSwift）からToDoのデータを取得する
    ///
        func ToDoFromDatabase() -> Results<ToDo> {
        do {
            let realm = try Realm()
            
            return realm.objects(ToDo.self).sorted(byKeyPath: "time", ascending: false)
            

        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
    }
    

    

}
