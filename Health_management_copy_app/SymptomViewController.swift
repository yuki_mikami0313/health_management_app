//
//  SymptomViewController.swift
//  Health_management_copy_app
//
//  Created by ymikami on 2020/05/11.
//  Copyright © 2020 ymikami. All rights reserved.
//

import UIKit

class SymptomViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
//    var rowListArray = [Int]()
//    var itemListArray = [Int]()
//    var sortArray = [Int]()
//    var aListArray = [String]()
//    var stringListArray = [String]()
    
    var selectedRowNumbersList = [Int]()


    @IBOutlet weak var tableView: UITableView!
    
    let items = ["鼻水","鼻づまり","咳","たん","くしゃみ","のどの痛み","鼻水","頭痛"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         

       

        // 複数選択を有効にする
        
        
        tableView.dataSource = self
               tableView.delegate = self

               // trueで複数選択、falseで単一選択
               tableView.allowsMultipleSelection = true

               
    }
    //    必須メソッド_行数の設定
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
            return items.count
        }
        
    //    必須メソッド_埋め込むセルの値を設定
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
            let cell = UITableViewCell()
            cell.backgroundColor = UIColor.clear
            cell.textLabel?.text = items[indexPath.row]
            //  セルの選択時の背景色を消す
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            //  セルの選択状況の判定
            if (selectedRowNumbersList.contains(indexPath.row)){
                cell.accessoryType = .checkmark
            }else{
                cell.accessoryType = .none
            }
            return cell
        }
     
    //    選択時の動作
         func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
             guard let cell = tableView.cellForRow(at: indexPath) else { return }
             switch cell.accessoryType {
             // 選択したセルにチェックマークが無い場合
             case .none:
                 cell.accessoryType = .checkmark
                 selectedRowNumbersList.append(indexPath.row)
             // 選択したセルにチェックマークがある場合
             case .checkmark:
                 cell.accessoryType = .none
                 selectedRowNumbersList = selectedRowNumbersList.filter ({ $0 != indexPath.row })
             case .detailButton, .detailDisclosureButton, .disclosureIndicator:
                 break
             @unknown default:
                 fatalError()
             }
         }
    
    // ①セグエ実行前処理
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
           // ②Segueの識別子確認
           if segue.identifier == "toView2" {
    
               // ③遷移先ViewCntrollerの取得
               let nextView = segue.destination as! secondViewController
    
               // ④値の設定
            nextView.symptom = items.enumerated().filter({ selectedRowNumbersList.contains($0.0) }).map { $0.1 }}
            
           
       }
    @IBAction func symptomT(_ sender: Any) {
        // 画面を閉じる
        self.dismiss(animated: true,completion: nil)
    }
    @IBAction func modoru(_ sender: Any) {
        self.dismiss(animated: true,completion: nil)
    }
}

