import UIKit
import Foundation

// ①・・・RealmSwiftをimport
import RealmSwift
import Charts


class ViewController: UIViewController  {
    
//    var itemList: Results<ToDo>!
//
//    var time: [String] = ["","2","3","4","5","6","7"]

    
    @IBOutlet weak var containerlineView: UIView!
    
    @IBOutlet weak var containerlinemonth: UIView!
    @IBOutlet weak var containerlineday: UIView!
    @IBOutlet weak var containertableView: UIView!
    @IBOutlet weak var containerday: UIView!
    
    @IBOutlet weak var containermonth: UIView!
    var containers: Array<UIView> = []
    var linecontainers: Array<UIView> = []
    
    


  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    containers = [containerday,containermonth]
    
    linecontainers = [containerlineday,containerlinemonth]
    containertableView.bringSubviewToFront(containerday)
    
    containerlineView.bringSubviewToFront(containerlineday)
    
    
    
//    tableView.delegate = self as UITableViewDelegate
//    tableView.dataSource = self as UITableViewDataSource
    
    //tableView.transform = tableView.transform.rotated(by: CGFloat(Double.pi))
    
    
    
    //チャートをアップデート
    
//  updateChartWithData()


    
    // Realmからデータを取得
//           do{
//               let realm = try Realm()
//               itemList = realm.objects(ToDo.self)
//            //TableView日付順
//            itemList = realm.objects(ToDo.self).sorted(byKeyPath: "time", ascending: false)
//           }catch{
//           }
    // tableViewにカスタムセルを登録
//    tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
    
   
  }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func changeContainerView(_ sender: UISegmentedControl) {
           let currentContainerView = containers[sender.selectedSegmentIndex]
        
        containertableView.bringSubviewToFront(currentContainerView)
       }
    
    
    @IBAction func changeContainer2View(_ sender: UISegmentedControl) {
        let currentContainerView2 = linecontainers[sender.selectedSegmentIndex]
        containerlineView.bringSubviewToFront(currentContainerView2)
    }
    

}
