//
//  line1ViewController.swift
//  Health_management_copy_app
//
//  Created by ymikami on 2020/04/22.
//  Copyright © 2020 ymikami. All rights reserved.
//
import Foundation
import UIKit
import Charts
import RealmSwift

class line1ViewController: UIViewController {
    var itemList: Results<ToDo>!
    
    

    var time: [String] = ["今日","1日前","2日前","3日前","4日前","5日前","6日前"]

    @IBOutlet weak var lineView: LineChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        updateChartWithData()
        
        do{
                        let realm = try Realm()
                          itemList = realm.objects(ToDo.self)
                      
                      }catch{
                      }
        lineView.notifyDataSetChanged()
    }
    //   画面が表示される直前にViewを更新
       override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           updateChartWithData()
           
       }
    /// チャート更新
        func updateChartWithData() {





            let xaxis = lineView.xAxis
            let leftAxis = lineView.leftAxis
            let rightAxis = lineView.rightAxis
    //        xaxis.labelPosition = .bottom
            xaxis.valueFormatter = IndexAxisValueFormatter(values:time)
            xaxis.granularity = 1
            xaxis.drawLabelsEnabled = true
            xaxis.drawLimitLinesBehindDataEnabled = true
            xaxis.avoidFirstLastClippingEnabled = true
            xaxis.gridLineDashLengths = [10, 10]

            leftAxis.gridLineDashLengths = [5, 5]
            
            rightAxis.enabled = true //右軸(値)の表示
            leftAxis.enabled = false //左軸（値)の表示
    //        lineView.leftAxis.labelCount = Int(7)//y軸ラベルの表示数


            lineView.noDataText = "データなし" //Noデータ時に表示する文字

            //データ配列を定義・・・（１）
            var dataEntries: [ChartDataEntry] = []


            //データベースから取得
            let toDo = ToDoFromDatabase()

            // 取得したデータを（１）のデータ配列に設定

            if toDo.count <  7{
                for i in (0..<toDo.count) {
    //        .reversed()


                let dataEntry = ChartDataEntry(x: Double(i), y: Double(toDo[i].netu)!)
                dataEntries.append(dataEntry)

            }
            }else{
                for i in (0..<7) {
    //             .reversed()


                     let dataEntry = ChartDataEntry(x: Double(i), y: Double(toDo[i].netu)!)
                     dataEntries.append(dataEntry)
            }
            }
            

            // チャート情報にラベルとデータを設定
            let chartDataSet = LineChartDataSet(entries: dataEntries, label: "体温")
            chartDataSet.colors = [NSUIColor.blue]
            chartDataSet.circleColors = [UIColor.red]
            chartDataSet.valueFormatter = LineChartValueFormatter()
            xaxis.labelCount = toDo.count//x軸ラベルの表示数


            let chartData = LineChartData(dataSet: chartDataSet)
            print(time)



            // viewにチャートデータを設定
            lineView.data = chartData


    }
    
    
    ///データベース（RealmSwift）からToDoのデータを取得する
        func ToDoFromDatabase() -> Results<ToDo> {
        do {
            let realm = try Realm()
            return realm.objects(ToDo.self).sorted(byKeyPath: "time", ascending: false)
            
            


        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
            
    }

    public class LineChartValueFormatter: NSObject, IValueFormatter{
        func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
            return String(format: "%0.1f", value)
        }
    }
    
    

}
